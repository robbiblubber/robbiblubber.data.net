﻿using System;



namespace Robbiblubber.Data.SQL
{
    /// <summary>This class provides a simple implementation of the ISQLField interface.</summary>
    public class SQLField: ISQLColumn
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SQLField(): this(string.Empty, string.Empty, string.Empty)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="fieldName">Field name.</param>
        public SQLField(string fieldName): this()
        {
            if(fieldName.Contains("."))
            {
                ColumnName = fieldName.Substring(fieldName.IndexOf(".") + 1);
                TableAlias = fieldName.Substring(0, fieldName.IndexOf("."));
            }
            else
            {
                ColumnName = fieldName;
            }
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="tableAlias">Table alias.</param>
        /// <param name="fieldName">Field name.</param>
        /// <param name="fieldAlias">Field alias.</param>
        public SQLField(string tableAlias, string fieldName, string? fieldAlias = null)
        {
            TableAlias = tableAlias;
            ColumnName = fieldName;
            ColumnAlias = fieldAlias;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ISQLField                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the table alias.</summary>
        public virtual string? TableAlias { get; set; }


        /// <summary>Gets or sets the field name.</summary>
        public virtual string ColumnName { get; set; }


        /// <summary>Gets or sets the field alias.</summary>
        public virtual string? ColumnAlias { get; set; }


        /// <summary>Gets the column expression as required in a where clause.</summary>
        public virtual string ColumnExpression
        {
            get
            {
                if(string.IsNullOrWhiteSpace(TableAlias)) { return ColumnName; }

                return TableAlias + "." + ColumnName;
            }
        }
    }
}
