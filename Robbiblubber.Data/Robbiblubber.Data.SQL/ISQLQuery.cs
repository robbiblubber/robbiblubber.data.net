﻿using System;



namespace Robbiblubber.Data.SQL
{
    /// <summary>Classes that build SQL queries implement this interface.</summary>
    public interface ISQLQuery
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the SQL text.</summary>
        public string Text { get; }


        /// <summary>Gets or sets the parameters for the query.</summary>
        public List<(string Name, object Value)> Parameters { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a copy of this SQL query.</summary>
        /// <returns>Cloned SQL query.</returns>
        public ISQLQuery Clone();


        /// <summary>Adds a primary query table to the query.</summary>
        /// <param name="tab">Table.</param>
        /// <returns>SQL query.</returns>
        public ISQLQuery From(ISQLTable tab);


        /// <summary>Adds selection fields to the query.</summary>
        /// <param name="field">Field.</param>
        /// <returns>SQL query.</returns>
        public ISQLQuery Select(params ISQLColumn[] field);


        /// <summary>Makes the query distinct.</summary>
        /// <returns>SQL query.</returns>
        public ISQLQuery Distinct();


        /// <summary>Adds a LEFT OUTER JOIN to the query.</summary>
        /// <param name="tab">Table.</param>
        /// <param name="cond">Join conditions.</param>
        /// <returns>SQL query.</returns>
        public ISQLQuery LeftOuterJoin(ISQLTable tab, params string[] cond);


        /// <summary>Adds a RIGHT OUTER JOIN to the query.</summary>
        /// <param name="tab">Table.</param>
        /// <param name="cond">Join conditions.</param>
        /// <returns>SQL query.</returns>
        public ISQLQuery RightOuterJoin(ISQLTable tab, params string[] cond);


        /// <summary>Adds a FULL OUTER JOIN to the query.</summary>
        /// <param name="tab">Table.</param>
        /// <param name="cond">Join conditions.</param>
        /// <returns>SQL query.</returns>
        public ISQLQuery FullOuterJoin(ISQLTable tab, params string[] cond);


        /// <summary>Adds an INNER JOIN to the query.</summary>
        /// <param name="tab">Table.</param>
        /// <param name="cond">Join conditions.</param>
        /// <returns>SQL query.</returns>
        public ISQLQuery Join(ISQLTable tab, params string[] cond);


        /// <summary>Adds a WHERE condition to the query.</summary>
        /// <param name="cond">Where conditions.</param>
        /// <returns>SQL query.</returns>
        public ISQLQuery Where(params string[] cond);


        /// <summary>Adds an AND where condition to the query.</summary>
        /// <param name="cond">Where conditions.</param>
        /// <returns>SQL query.</returns>
        public ISQLQuery And(params string[] cond);


        /// <summary>Adds an OR where condition to the query.</summary>
        /// <param name="cond">Where conditions.</param>
        /// <returns>SQL query.</returns>
        public ISQLQuery Or(params string[] cond);


        /// <summary>Adds ORDER BY clauses to the query.</summary>
        /// <param name="field">Fields.</param>
        /// <returns>SQL Query.</returns>
        public ISQLQuery OrderBy(params ISQLColumn[] field);


        /// <summary>Adds ORDER BY DESC clauses to the query.</summary>
        /// <param name="field">Fields.</param>
        /// <returns>SQL Query.</returns>
        public ISQLQuery OrderByDesc(params ISQLColumn[] field);


        /// <summary>Adds GROUP BY clauses to the query.</summary>
        /// <param name="field">Fields.</param>
        /// <returns>SQL Query.</returns>
        public ISQLQuery GroupBy(params ISQLColumn[] field);


        /// <summary>Adds HAVING clauses to the query.</summary>
        /// <param name="cond">Having conditions.</param>
        /// <returns>SQL Query.</returns>
        public ISQLQuery Having(params string[] cond);


        /// <summary>Adds a parameter to the query.</summary>
        /// <param name="name">Parameter name.</param>
        /// <param name="value">Parameter value.</param>
        /// <returns>SQL query.</returns>
        public ISQLQuery AddParameter(string name, object value);


        /// <summary>Adds a UNION statement to the query.</summary>
        /// <param name="query">SQL query.</param>
        /// <returns>SQL query.</returns>
        public ISQLQuery Union(ISQLQuery query);


        /// <summary>Adds a UNION ALL statement to the query.</summary>
        /// <param name="query">SQL query.</param>
        /// <returns>SQL query.</returns>
        public ISQLQuery UnionAll(ISQLQuery query);
    }
}
