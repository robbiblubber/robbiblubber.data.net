﻿using System;



namespace Robbiblubber.Data.SQL
{
    /// <summary>This class provides a simple implementation of a SQL table.</summary>
    public class SQLTable: ISQLTable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="tableAlias">Table alias.</param>
        public SQLTable(string tableName, string? tableAlias = null)
        {
            TableName = tableName;
            TableAlias = tableAlias;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the table name.</summary>
        public virtual string TableName { get; set; }


        /// <summary>Gets or sets the table alias.</summary>
        public virtual string? TableAlias { get; set; }
    }
}
