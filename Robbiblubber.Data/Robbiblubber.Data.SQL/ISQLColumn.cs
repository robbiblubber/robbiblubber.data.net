﻿using System;



namespace Robbiblubber.Data.SQL
{
    /// <summary>Classes representing columns in a SQL query implement this interface.</summary>
    public interface ISQLColumn
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the table alias.</summary>
        public string? TableAlias { get; }


        /// <summary>Gets the column name.</summary>
        public string ColumnName { get; }


        /// <summary>Gets the column alias.</summary>
        public string? ColumnAlias { get; }


        /// <summary>Gets the column expression as required in a where clause.</summary>
        public string ColumnExpression { get; }
    }
}
