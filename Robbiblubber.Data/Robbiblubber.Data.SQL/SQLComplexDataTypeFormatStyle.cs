﻿using System;
using System.Collections.Generic;



namespace Robbiblubber.Data.SQL
{
    /// <summary>This enumeration defines format styles for complex data type representation.</summary>
    public enum SQLComplexDataTypeFormatStyle
    {
        /// <summary>Default data representation.</summary>
        DEFAULT = 0,
        /// <summary>Data representation using assignments without quotes.</summary>
        ASSIGNMENT_NO_QUOTES = 0,
        /// <summary>Data representation using assignments with quotes.</summary>
        ASSIGNMENT_WITH_QUOTES = 1,
        /// <summary>JSON data representation.</summary>
        JSON = 200
    }
}
