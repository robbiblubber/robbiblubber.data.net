﻿using System;



namespace Robbiblubber.Data.SQL
{
    /// <summary>SQL Statement classes implement this interface.</summary>
    public interface ISQLStatement
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the statement source.</summary>
        public string Source { get; set; }


        /// <summary>Gets if the statement is DDL.</summary>
        public bool IsDDL { get; }


        /// <summary>Gets if the statement is commit.</summary>
        public bool IsCommit { get; }


        /// <summary>Gets if the statement is rollback.</summary>
        public bool IsRollback { get; }


        /// <summary>Gets if the statement is a SELECT query.</summary>
        public bool IsSelect { get; }


        /// <summary>Gets if the statement is an INSERT statement.</summary>
        public bool IsInsert { get; }


        /// <summary>Gets if the statement is an UPDATE statement.</summary>
        public bool IsUpdate { get; }


        /// <summary>Gets if the statement is a DELETE statement.</summary>
        public bool IsDelete { get; }


        /// <summary>Gets if the statement is an INSERT statement.</summary>
        public bool IsDML { get; }
    }
}
