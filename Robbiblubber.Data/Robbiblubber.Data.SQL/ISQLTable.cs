﻿using System;



namespace Robbiblubber.Data.SQL
{
    /// <summary>Classes representing tables in a SQL query implement this interface.</summary>
    public interface ISQLTable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the table name.</summary>
        public string TableName { get; }


        /// <summary>Gets the table alias.</summary>
        public string? TableAlias { get; }
    }
}
