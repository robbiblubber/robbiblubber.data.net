﻿using System;
using System.Data;

using Robbiblubber.Spellbook;



namespace Robbiblubber.Data
{
    /// <summary>This class provides extension methods for data retrieval from data readers.</summary>
    public static class SQLRetrievalExtensions
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public extension methods                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets if a column value is NULL.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="i">Column index.</param>
        /// <returns>Returns TRUE if the value is NULL, otherwise returns FALSE.</returns>
        public static bool IsNull(this IDataReader re, int i)
        {
            return re.IsDBNull(i);
        }


        /// <summary>Gets if a column value is NULL.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="c">Column name.</param>
        /// <returns>Returns TRUE if the value is NULL, otherwise returns FALSE.</returns>
        public static bool IsNull(this IDataReader re, string c)
        {
            return re.IsDBNull(re.GetOrdinal(c));
        }


        /// <summary>Gets a value from a reader.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="re">Reader.</param>
        /// <param name="i">Column index.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Returns the value from the data reader.</returns>
        public static T Get<T>(this IDataReader re, int i, T defaultValue)
        {
            if(re.IsDBNull(i)) { return defaultValue; }

            if(typeof(T) == typeof(string)) { return (T)(object) Convert.ToString(re.GetValue(i))!; }
            if(typeof(T) == typeof(int) || typeof(T) == typeof(int?)) { return (T)(object) Convert.ToInt32(re.GetValue(i))!; }
            if(typeof(T) == typeof(long) || typeof(T) == typeof(long?)) { return (T)(object) Convert.ToInt64(re.GetValue(i))!; }
            if(typeof(T) == typeof(short) || typeof(T) == typeof(short?)) { return (T)(object) Convert.ToInt16(re.GetValue(i))!; }
            if(typeof(T) == typeof(byte) || typeof(T) == typeof(byte?)) { return (T)(object) Convert.ToByte(re.GetValue(i))!; }
            if(typeof(T) == typeof(uint) || typeof(T) == typeof(uint?)) { return (T)(object) Convert.ToUInt32(re.GetValue(i))!; }
            if(typeof(T) == typeof(ulong) || typeof(T) == typeof(ulong?)) { return (T)(object) Convert.ToUInt64(re.GetValue(i))!; }
            if(typeof(T) == typeof(ushort) || typeof(T) == typeof(ushort?)) { return (T)(object) Convert.ToUInt16(re.GetValue(i))!; }
            if(typeof(T) == typeof(sbyte) || typeof(T) == typeof(sbyte?)) { return (T)(object) Convert.ToSByte(re.GetValue(i))!; }
            if(typeof(T) == typeof(float) || typeof(T) == typeof(float?)) { return (T)(object) Convert.ToSingle(re.GetValue(i))!; }
            if(typeof(T) == typeof(double) || typeof(T) == typeof(double?)) { return (T)(object) Convert.ToDouble(re.GetValue(i))!; }
            if(typeof(T) == typeof(decimal) || typeof(T) == typeof(decimal?)) { return (T)(object) Convert.ToDecimal(re.GetValue(i))!; }

            if(typeof(T) == typeof(bool) || typeof(T) == typeof(bool?))
            {
                object rval = re.GetValue(i);

                if(rval.GetType() == typeof(bool)) { return (T) rval; }
                if(rval.GetType() == typeof(string)) { return (T)(object) StringOp.ToBoolean((string) rval); }

                return (T)(object) (Convert.ToInt64(rval) != 0);
            }

            if(typeof(T) == typeof(DateTime)) { return (T)(object) Convert.ToDateTime((re.GetValue(i))); }

            return (T) re.GetValue(i);
        }


        /// <summary>Gets a value from a reader.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="re">Reader.</param>
        /// <param name="i">Column index.</param>
        /// <returns>Returns the value from the data reader.</returns>
        public static T Get<T>(this IDataReader re, int i)
        {
            return Get<T>(re, i, default!);
        }


        /// <summary>Gets a value from a reader.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="re">Reader.</param>
        /// <param name="c">Column name.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Returns the value from the data reader.</returns>
        public static T Get<T>(this IDataReader re, string c, T defaultValue)
        {
            return Get<T>(re, re.GetOrdinal(c), defaultValue);
        }


        /// <summary>Gets a value from a reader.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="re">Reader.</param>
        /// <param name="c">Column name.</param>
        /// <returns>Returns the value from the data reader.</returns>
        public static T Get<T>(this IDataReader re, string c)
        {
            return Get<T>(re, re.GetOrdinal(c), default!);
        }
    }
}
