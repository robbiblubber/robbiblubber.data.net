﻿using System;
using System.Collections;
using System.Data;
using System.Net;
using System.Text;

using Robbiblubber.Data.SQL;
using Robbiblubber.Data.DDL;
using Robbiblubber.Spellbook;



namespace Robbiblubber.Data.Providers
{
    /// <summary>Provides SQL Parser functionality.</summary>
    public class DbParser: IDbParser
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets if complex data types should be displayed with line breaks.</summary>
        public static bool ComplexDataTypeMultiLine
        {
            get; set;
        } = true;


        /// <summary>Gets or sets the minimum length for a data type representation to toggle wrap.</summary>
        public static int ComplexDataTypeWidth
        {
            get; set;
        } = 60;


        /// <summary>Gets or sets the representation format for complex data types.</summary>
        public static SQLComplexDataTypeFormatStyle ComplexDataTypeFormat
        {
            get; set;
        } = SQLComplexDataTypeFormatStyle.DEFAULT;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IParser                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a well-formatted query function call for the current timestamp.</summary>
        /// <returns>Timestamp query string.</returns>
        public virtual string CurrentTimestampFunction
        {
            get { return "Current_Timestamp"; }
        }


        /// <summary>Gets the database concatination operator.</summary>
        public virtual string ConcatOperator
        {
            get { return "||"; }
        }


        /// <summary>Gets the database table as operator.</summary>
        public virtual string AsOperator
        {
            get { return " "; }
        }


        /// <summary>Gets the add operator.</summary>
        public virtual string AddOperator
        {
            get { return "+"; }
        }


        /// <summary>Gets the subtraction operator.</summary>
        public virtual string SubtractOperator
        {
            get { return "-"; }
        }


        /// <summary>Gets the multiplication operator.</summary>
        public virtual string MultiplyOperator
        {
            get { return "*"; }
        }


        /// <summary>Gets the division operator.</summary>
        public virtual string DivideOperator
        {
            get { return "/"; }
        }


        /// <summary>Gets the modulo operator.</summary>
        public virtual string ModuloOperator
        {
            get { return "%"; }
        }


        /// <summary>Gets the negation operator.</summary>
        public virtual string NegateOperator
        {
            get { return "-"; }
        }


        /// <summary>Gets the binary or operator.</summary>
        public virtual string BinaryOrOperator
        {
            get { return "|"; }
        }


        /// <summary>Gets the binary and operator.</summary>
        public virtual string BinaryAndOperator
        {
            get { return "&"; }
        }


        /// <summary>Gets the exclusive or operator.</summary>
        public virtual string ExOrOperator
        {
            get { return "^"; }
        }


        /// <summary>Gets the not operator.</summary>
        public virtual string NotOperator
        {
            get { return "NOT"; }
        }


        /// <summary>Gets the logical or operator.</summary>
        public virtual string OrOperator
        {
            get { return "OR"; }
        }


        /// <summary>Gets the logical and operator.</summary>
        public virtual string AndOperator
        {
            get { return "AND"; }
        }


        /// <summary>Gets the equal operator.</summary>
        public virtual string EqualOperator
        {
            get { return "="; }
        }


        /// <summary>Gets the equals null operator.</summary>
        public virtual string EqualNullOperator
        {
            get { return "IS"; }
        }


        /// <summary>Gets the not equal operator.</summary>
        public virtual string NotEqualOperator
        {
            get { return "<>"; }
        }


        /// <summary>Gets the not equals null operator.</summary>
        public virtual string NotEqualNullOperator
        {
            get { return "IS NOT"; }
        }


        /// <summary>Gets the greater than operator.</summary>
        public virtual string GreaterOperator
        {
            get { return ">"; }
        }


        /// <summary>Gets the greater than or equal operator.</summary>
        public virtual string GreaterOrEqualOperator
        {
            get { return ">="; }
        }


        /// <summary>Gets the less than operator.</summary>
        public virtual string LessOperator
        {
            get { return "<"; }
        }


        /// <summary>Gets the less than or equal operator.</summary>
        public virtual string LessOrEqualOperator
        {
            get { return "<="; }
        }


        /// <summary>Gets the specific FROM part for dual selects.</summary>
        public virtual string FromDual
        {
            get { return ""; }
        }


        /// <summary>Gets a well-formatted query function call for last auto increment value.</summary>
        public virtual string LastValue
        {
            get { throw new NotSupportedException(); }
        }


        /// <summary>Parses a source string.</summary>
        /// <param name="source">Source string.</param>
        /// <returns>Parsed SQL statements.</returns>
        public virtual ISQLStatement[] Parse(string source)
        {
            List<string> strings = _Parse(source);

            ISQLStatement[] rval = new ISQLStatement[strings.Count];

            for(int i = 0; i < strings.Count; i++)
            {
                rval[i] = new SQLStatement(strings[i]);
            }

            return rval;
        }


        /// <summary>Creates a query object.</summary>
        /// <returns>Query.</returns>
        public virtual ISQLQuery CreateQuery()
        {
            return new SQLQuery(this);
        }



        /// <summary>Reads a field value as a string.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="i">Index.</param>
        /// <param name="style">Data format.</param>
        /// <param name="multiLine">Multi-line flag.</param>
        /// <param name="width">Line width.</param>
        /// <returns>String.</returns>
        public virtual string GetDataString(IDataReader re, int i, SQLComplexDataTypeFormatStyle style, bool multiLine, int width)
        {
            try
            {
                if(re.IsDBNull(i)) return "";

                return ToString(re.GetValue(i), style, multiLine, width);
            }
            catch(Exception) { }

            return "(e?)";
        }


        /// <summary>Reads a field value as a string.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="i">Index.</param>
        /// <returns>String.</returns>
        public virtual string GetDataString(IDataReader re, int i)
        {
            return GetDataString(re, i, ComplexDataTypeFormat, ComplexDataTypeMultiLine, ComplexDataTypeWidth);
        }


        /// <summary>Returns if an object is a string.</summary>
        /// <param name="obj">Object</param>
        /// <returns>Returns FALSE if the object is a numeric datatype, otherwise returns TRUE.</returns>
        public virtual bool IsString(object obj)
        {
            return !((obj is int) || (obj is uint) || (obj is long) || (obj is ulong) || (obj is short) || (obj is ushort) || (obj is byte) || (obj is float) || (obj is double) || (obj is decimal));
        }


        /// <summary>Returns a string representation of an object.</summary>
        /// <param name="obj">Object</param>
        /// <returns>String.</returns>
        public virtual string ToString(object obj)
        {
            return ToString(obj, ComplexDataTypeFormat, ComplexDataTypeMultiLine, ComplexDataTypeWidth);
        }


        /// <summary>Returns a string representation of an object.</summary>
        /// <param name="obj">Object</param>
        /// <returns>String.</returns>
        /// <param name="style">Data format.</param>
        /// <param name="multiLine">Multi-line flag.</param>
        /// <param name="width">Line width.</param>
        public virtual string ToString(object? obj, SQLComplexDataTypeFormatStyle style, bool multiLine, int width)
        {
            if(obj == null) { return string.Empty; }
            if(obj is string) { return (string) obj; }
            if(obj is int) { return obj.ToString() ?? string.Empty; }
            if(obj is uint) { return obj.ToString() ?? string.Empty; }
            if(obj is long) { return obj.ToString() ?? string.Empty; }
            if(obj is ulong) { return obj.ToString() ?? string.Empty; }
            if(obj is short) { return obj.ToString() ?? string.Empty; }
            if(obj is ushort) { return obj.ToString() ?? string.Empty; }
            if(obj is char) { return obj.ToString() ?? string.Empty; }
            if(obj is byte) { return obj.ToString() ?? string.Empty; }
            if(obj is float) { return obj.ToString() ?? string.Empty; }
            if(obj is double) { return obj.ToString() ?? string.Empty; }
            if(obj is decimal) { return obj.ToString() ?? string.Empty; }
            if(obj is bool) { return (((bool) obj) ? "true" : "false"); }

            if(obj is DateTime) { return ((DateTime) obj).ToTimestamp(); }
            if(obj is Guid) { return ((Guid) obj).ToString(); }
            if(obj is IPAddress) { return ((IPAddress) obj).ToString(); }

            if(obj is IEnumerable<byte>) { return Hex.FromBytes((IEnumerable<byte>) obj); }


            if(_IsDictionary(obj))
            {
                List<KeyValuePair<Tuple<string, object>, Tuple<string, object>>> vals = new List<KeyValuePair<Tuple<string, object>, Tuple<string, object>>>();

                foreach(object i in (IEnumerable) obj)
                {
                    vals.Add(new KeyValuePair<Tuple<string, object>, Tuple<string, object>>(new Tuple<string, object>(ToString(i.__Get<object>("Key"), style, multiLine, width), i.__Get<object>("Key") ?? new object()), new Tuple<string, object>(ToString(i.__Get<object>("Value") ?? new object(), style, multiLine, width), i.__Get<object>("Value") ?? new object())));
                }

                if(style == SQLComplexDataTypeFormatStyle.JSON)
                {
                    bool first = true;
                    string rval = "";
                    string bufv = "{";
                    foreach(KeyValuePair<Tuple<string, object>, Tuple<string, object>> j in vals)
                    {
                        string curv = "";
                        if(first) { first = false; }
                        else
                        {
                            bufv += ", ";
                        }

                        if(IsString(j.Key.Item2))
                        {
                            curv += ("'" + j.Key.Item1.Replace("'", "\\'") + "': ");
                        }
                        else { curv += (j.Key.Item1 + ": "); }

                        if(IsString(j.Value.Item2))
                        {
                            curv += ("'" + j.Value.Item1.Replace("'", "\\'") + "'");
                        }
                        else { curv += (j.Value.Item1); }

                        if(multiLine && ((bufv.Length + curv.Length) > width))
                        {
                            if(rval.Length > 0) { rval += "\r\n "; }
                            rval += bufv;
                            bufv = curv;
                        }
                        else { bufv += curv; }
                    }

                    if(bufv.Length > 0)
                    {
                        if(multiLine && (rval.Length > 0)) { rval += "\r\n "; }
                        rval += bufv;
                    }
                    rval += "}";

                    return rval;
                }
                else
                {
                    StringBuilder rval = new StringBuilder();
                    int lk = 1;
                    if(vals.Count > 0) { lk = vals.Max(m => m.Key.Item1.Length) + 1; }

                    bool first = true;
                    string br = "", nobr = "";
                    foreach(KeyValuePair<Tuple<string, object>, Tuple<string, object>> j in vals)
                    {
                        if(first) { first = false; }
                        else
                        {
                            br += ",\r\n";
                            nobr += ", ";
                        }

                        br += j.Key.Item1.PadRight(lk, ' ');
                        br += " => ";
                        nobr += j.Key.Item1;
                        nobr += " => ";

                        if(IsString(j.Value.Item2) && (style != SQLComplexDataTypeFormatStyle.ASSIGNMENT_NO_QUOTES))
                        {
                            br += ("\"" + j.Value.Item1.Replace("\"", "\\\"").Replace("\r\n", "\r").Replace("\r", "\n").Replace("\n", "\r\n".PadRight(lk + 3)) + "\"");
                            nobr += ("\"" + j.Value.Item1.Replace("\"", "\\\"") + "\"");
                        }
                        else
                        {
                            br += j.Value.Item1.Replace("\r\n", "\r").Replace("\r", "\n").Replace("\n", "\r\n".PadRight(lk + 3));
                            nobr += j.Value.Item1;
                        }
                    }

                    if(multiLine && (nobr.Length >= width))
                    {
                        rval.Append(br);
                    }
                    else { rval.Append(nobr); }

                    return rval.ToString();
                }
            }

            if(obj is IEnumerable)
            {
                string rval = "";
                string bufv = "";
                bool first = true;

                if(style == SQLComplexDataTypeFormatStyle.JSON) { bufv += "["; }

                foreach(object i in (IEnumerable) obj)
                {
                    string curv = "";
                    if(first) { first = false; }
                    else
                    {
                        bufv += ", ";
                    }

                    switch(style)
                    {
                        case SQLComplexDataTypeFormatStyle.JSON:
                            if(IsString(i)) { curv += ("'" + ToString(i, style, multiLine, width).Replace("'", "\\'") + "'"); } else { curv += ToString(i, style, multiLine, width); }
                            break;
                        case SQLComplexDataTypeFormatStyle.ASSIGNMENT_NO_QUOTES:
                            curv += ToString(i, style, multiLine, width); break;
                        default:
                            if(IsString(i)) { curv += ("\"" + ToString(i, style, multiLine, width).Replace("\"", "\\\"") + "\""); } else { curv += ToString(i, style, multiLine, width); }
                            break;
                    }

                    if(multiLine && ((bufv.Length + curv.Length) > width))
                    {
                        if(rval.Length > 0) { rval += "\r\n  "; }
                        rval += bufv;
                        bufv = curv;
                    }
                    else { bufv += curv; }
                }

                if(bufv.Length > 0)
                {
                    if(multiLine && (rval.Length > 0)) { rval += "\r\n "; }
                    rval += bufv;
                }
                if(style == SQLComplexDataTypeFormatStyle.JSON) { rval += "]"; }

                return rval;
            }

            return (obj.ToString() ?? string.Empty);
        }


        /// <summary>Returns a well-formatted query function call for next value from the given database sequence.</summary>
        /// <param name="sequence">Sequence Name.</param>
        /// <returns>Sequence query string.</returns>
        public virtual string SequenceNextValue(string sequence)
        {
            return "NextVal('" + sequence + "')";
        }


        /// <summary>Adds a time interval to a database date/time expression.</summary>
        /// <param name="value">Database expression that represents a date/time value.</param>
        /// <param name="interval">Time interval to add.</param>
        /// <returns>Database timestamp operation string.</returns>
        public virtual string AddTimeInterval(string value, TimeSpan interval)
        {
            string pre = ((interval < TimeSpan.Zero) ? " - INTERVAL '" : " + INTERVAL '");

            if(interval.Seconds == 0)
            {
                return value + pre + Math.Abs(interval.TotalMinutes).ToString() + " minutes'";
            }
            else
            {
                return value + pre + Math.Abs(interval.TotalSeconds).ToString() + " seconds'";
            }
        }


        /// <summary>Returns the database lowercase value for a given expression.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Lowercase expression.</returns>
        public virtual string Lowercase(string expression)
        {
            return "Lower(" + expression + ")";
        }


        /// <summary>Returns the database lowercase value for a given expression.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Lowercase expression.</returns>
        public virtual string Uppercase(string expression)
        {
            return "Upper(" + expression + ")";
        }


        /// <summary>Creates a bind variable name using the given variable name.</summary>
        /// <param name="name">Variable name.</param>
        /// <returns>Bind variable name.</returns>
        public virtual string ToBindVariableName(string name)
        {
            return ':' + name.ToLower().Replace('_', 'm');
        }


        /// <summary>Returns a database data type name for a data type.</summary>
        /// <param name="dataType">Data type.</param>
        /// <returns>Data type name.</returns>
        public virtual string ToDataTypeName(SQLDataType dataType)
        {
            switch(dataType.ShortName)
            {
                case "s":
                case "sd":
                case "sx": return ((dataType.Length == 0) ? "TEXT" : ("VARCHAR(" + dataType.Length.ToString() + ")"));
                case "t": return "TIMESTAMP";
                case "b": return "BOOLEAN";
                case "ib": return "INTEGER";
                case "i":
                    if(dataType.Length == 8) return "SMALLINT";
                    if(dataType.Length == 16) return "SMALLINT";
                    if(dataType.Length == 64) return "BIGINT";
                    return "INTEGER";
                case "ff": return "REAL";
                case "fd": return "DOUBLE PRECISION";
                case "fx": return "NUMERIC";
            }

            return "<ERROR>";
        }


        /// <summary>Parses a DDL statement.</summary>
        /// <param name="ddl">DDL statement.</param>
        /// <returns>Statement list.</returns>
        public virtual string[] ParseDDL(IDDLStatement ddl)
        {
            List<string> rval = new List<string>();
            int ckn = 1;

            if(ddl is IDDLTable)
            {
                string n = ("CREATE TABLE " + ((IDDLTable) ddl).TableName + "\r\n(\r\n");
                List<string> cols = new List<string>();

                foreach(IDDLColumn i in ((IDDLTable) ddl).Columns)
                {
                    cols.Add(i.ColumnName.PadRight(15, ' ') + " " + ToDataTypeName(i.DataType).PadRight(11, ' ') + " " + (i.NotNull ? "NOT NULL " + (string.IsNullOrWhiteSpace(i.DefaultValue) ? "" : ("  DEFAULT " + i.DefaultValue)) : ""));
                }

                foreach(IDDLConstraint i in ((IDDLTable) ddl).Constraints)
                {
                    if(i is IDDLPrimaryKeyConstraint)
                    {
                        string? k = i.ConstraintName;
                        if(string.IsNullOrWhiteSpace(k)) { k = ("PK_" + ((IDDLTable) ddl).TableName); }

                        k = ("CONSTRAINT " + k + " PRIMARY KEY (");
                        for(int j = 0; j < ((IDDLPrimaryKeyConstraint) i).ColumnNames.Length; j++)
                        {
                            if(j > 0) { k += ", "; }
                            k += ((IDDLPrimaryKeyConstraint) i).ColumnNames[j];
                        }
                        k += ")";

                        cols.Add(k);
                    }
                    else if(i is IDDLForeignKeyConstraint)
                    {
                        string? k = i.ConstraintName;
                        if(string.IsNullOrWhiteSpace(k)) { k = ("FK_" + ((IDDLTable) ddl).TableName + "_" + ((IDDLForeignKeyConstraint) i).ColumnName); }

                        k = ("CONSTRAINT " + k + " FOREIGN KEY (" + ((IDDLForeignKeyConstraint) i).ColumnName + ") REFERENCES " +
                            ((IDDLForeignKeyConstraint) i).ReferencedTableName + "(" + ((IDDLForeignKeyConstraint) i).ReferencedColumnName + ")");
                        cols.Add(k);
                    }
                    else if(i is IDDLCheckConstraint)
                    {
                        string? k = i.ConstraintName;
                        if(string.IsNullOrWhiteSpace(k)) { k = ("CK_" + ((IDDLTable) ddl).TableName + "_CHECK" + (ckn++).ToString()); }

                        k = ("CONSTRAINT " + k + " CHECK (" + ((IDDLCheckConstraint) i).Condition + ")");
                    }
                }

                bool first = true;
                foreach(string i in cols)
                {
                    if(first) { first = false; } else { n += ",\r\n"; }
                    n += ("   " + i.Trim());
                }
                n += "\r\n)";

                rval.Add(n);
            }
            else if(ddl is IDDLIndex)
            {
                string? n = ((IDDLIndex) ddl).IndexName;
                if(string.IsNullOrWhiteSpace(n))
                {
                    n = ((((IDDLIndex) ddl).Unique ? "UX_" : "IX_") + ((IDDLIndex) ddl).TableName);
                    foreach(string i in ((IDDLIndex) ddl).ColumnNames) { n += ("_" + i); }
                }

                n = ("CREATE " + (((IDDLIndex) ddl).Unique ? "UNIQUE INDEX " : "INDEX ") + n + " ON " + ((IDDLIndex) ddl).TableName + "(");
                for(int i = 0; i < ((IDDLIndex) ddl).ColumnNames.Length; i++)
                {
                    if(i > 0) { n += ", "; }
                    n += ((IDDLIndex) ddl).ColumnNames[i];
                }
                n += ")";

                rval.Add(n);
            }

            foreach(IDDLStatement i in ddl.ChildStatements) { rval.AddRange(ParseDDL(i)); }

            return rval.ToArray();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns if an object is a dictionary.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>Returns TRUE for dictionaries, FALSE for all other objects.</returns>
        protected virtual bool _IsDictionary(object obj)
        {
            if(obj == null) return false;

            if(obj is IDictionary) return true;
            foreach(Type i in obj.GetType().GetInterfaces())
            {
                if(i.Name.StartsWith("System.Collections.Generic.IDictionary")) return true;
            }
            return false;
        }


        /// <summary>Parses a source string.</summary>
        /// <param name="source">Source string.</param>
        /// <returns>Parsed SQL statement strings.</returns>
        protected virtual List<String> _Parse(string source)
        {
            List<string> rval = new List<string>();

            bool inStr = false;
            bool declare = false;
            int begins = 0;
            int pos = 0;

            while(true)
            {
                HitResult re = _HitNext(source, pos);

                if(re.Type == HitType.EOS)
                {
                    string s = source.TrimEnd(';', ' ', (char) 9, (char) 10, (char) 13).Trim();
                    if(s != "") { rval.Add(s); }
                    break;
                }
                else if(re.Type == HitType.SEMICOLON)
                {
                    if((begins == 0) && (!(declare || inStr)))
                    {
                        string s = source.Substring(0, re.End).TrimEnd(';', ' ', (char) 9, (char) 10, (char) 13).Trim();
                        if(s != "") { rval.Add(s); }
                        source = source.Substring(re.End);
                        pos = 0;
                    }
                    else { pos = re.End; }
                }
                else if(re.Type == HitType.APOSTROPHE)
                {
                    inStr = (!inStr);
                    pos = re.End;
                }
                else if(re.Type == HitType.DECLARE)
                {
                    declare = true;
                    pos = re.End;
                }
                else if(re.Type == HitType.BEGIN)
                {
                    declare = false;
                    begins++;
                    pos = re.End;
                }
                else if(re.Type == HitType.END)
                {
                    if(--begins == 0)
                    {
                        string s = source.Substring(0, re.End).TrimEnd(';', ' ', (char) 9, (char) 10, (char) 13).Trim();
                        if(s != "") { rval.Add(s); }
                        source = source.Substring(re.End);
                        pos = 0;
                    }
                }
            }

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected static methods                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the next special expression in the source string.</summary>
        /// <param name="source">Source string.</param>
        /// <param name="start">Starting position.</param>
        /// <returns>Hit information</returns>
        protected static HitResult _HitNext(string source, int start)
        {
            int pos = -1;
            int len = 0;
            int n = -1;
            HitType t = HitType.EOS;

            source = source.ToLower();

            if(start >= source.Length)
            {
                return new HitResult(HitType.EOS, start);
            }

            if((n = source.IndexOf(";", start)) > -1)
            {
                if((pos < 0) || (n < pos))
                {
                    pos = n;
                    len = 1;
                    t = HitType.SEMICOLON;
                }
            }

            if((n = source.IndexOf("'", start)) > -1)
            {
                if((pos < 0) || (n < pos))
                {
                    pos = n;
                    len = 1;
                    t = HitType.APOSTROPHE;
                }
            }

            if((n = source.IndexOf("declare", start)) > -1)
            {
                if((pos < 0) || (n < pos))
                {
                    pos = n;
                    len = 7;
                    t = HitType.DECLARE;
                }
            }

            if((n = source.IndexOf("begin", start)) > -1)
            {
                if((pos < 0) || (n < pos))
                {
                    pos = n;
                    len = 5;
                    t = HitType.BEGIN;
                }
            }

            if((n = source.IndexOf("end;", start)) > -1)
            {
                if((pos < 0) || (n < pos))
                {
                    pos = n;
                    len = 4;
                    t = HitType.END;
                }
            }

            return new HitResult(t, pos + len);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] HitResult                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This class implements a hit result.</summary>
        protected internal class HitResult
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Creates a new instance of this class.</summary>
            /// <param name="type">Type.</param>
            /// <param name="end">End position.</param>
            internal protected HitResult(HitType type, int end)
            {
                Type = type;
                End = end;
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // internal members                                                                                             //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Hit type.</summary>
            internal protected HitType Type { get; set; }


            /// <summary>End Position.</summary>
            internal int End { get; set; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [enum] HitType                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This enum defines possible hit types.</summary>
        protected internal enum HitType: int
        {
            /// <summary>Hit the end of string.</summary>
            EOS = 0,
            /// <summary>Hit a semicolon.</summary>
            SEMICOLON = 10,
            /// <summary>Hit an apostrophe.</summary>
            APOSTROPHE = 11,
            /// <summary>Hit a declare statement.</summary>
            DECLARE = 20,
            /// <summary>Hit a begin statement.</summary>
            BEGIN = 21,
            /// <summary>Hit an end statement.</summary>
            END = 29
        }
    }
}
