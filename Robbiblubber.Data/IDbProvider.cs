﻿using System;
using System.Data;



namespace Robbiblubber.Data
{
    /// <summary>Database providers implement this interface.</summary>
    public interface IDbProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the provider name.</summary>
        public string Name { get; }


        /// <summary>Gets the underlying database connection.</summary>
        public IDbConnection Connection { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Connects to the database.</summary>
        public void Connect();


        /// <summary>Disconnects from the database.</summary>
        public void Disconnect();


        /// <summary>Tests the connection.</summary>
        public void Test();
    }
}
