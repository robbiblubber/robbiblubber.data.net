﻿using System.Data;
using System.Text.RegularExpressions;

using Robbiblubber.Data.SQL;
using Robbiblubber.Spellbook;



namespace Robbiblubber.Data
{
    /// <summary>This class provides extension methods for database operations.</summary>
    public static class SQLExtensions
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private constants                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>This constant defines the parameter prefix for automatically created command parameters.</summary>
        private const string _PARAMETER_PREFIX = ":pam";

        /// <summary>This represents a masked value for opening curly braces.</summary>
        private static readonly string _MASKED_BRACE_OPEN = (char) 4 + "" + (char) 2;

        /// <summary>This represents a masked value for closing curly braces.</summary>
        private static readonly string _MASKED_BRACE_CLOSE = (char) 4 + "" + (char) 3;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public extension methods                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sets the command text and parameters for the command.</summary>
        /// <param name="command">Command.</param>
        /// <param name="commandText">Command text and parameters.</param>
        /// <returns>Returns the command object.</returns>
        public static IDbCommand SetCommandText(this IDbCommand command, FormattableString commandText)
        {
            int n = 0;
            command.CommandText = new Regex(@"(?<!{)\{(.+?)(?<!})\}").Replace(commandText.Format._ReplaceDoubles("{", _MASKED_BRACE_OPEN)._ReplaceDoubles("}", _MASKED_BRACE_CLOSE, false), m => _PARAMETER_PREFIX + n++).Replace(_MASKED_BRACE_OPEN, "{").Replace(_MASKED_BRACE_CLOSE, "}");

            for(int i = 0; i < commandText.ArgumentCount; i++)
            {
                command.AddParameter(_PARAMETER_PREFIX + i, commandText.GetArgument(i));
            }

            return command;
        }


        /// <summary>Sets the command text and parameters for the command.</summary>
        /// <param name="command">Command.</param>
        /// <param name="query">SQL query.</param>
        /// <returns>Returns the command object.</returns>
        public static IDbCommand SetCommandText(this IDbCommand command, ISQLQuery query)
        {
            command.CommandText = query.Text;
            return command.AddParameters(query.Parameters.ToArray());
        }


        /// <summary>Creates a command using a command text and parameters.</summary>
        /// <param name="cn">Database connection.</param>
        /// <param name="commandText">Command text and parameters.</param>
        /// <returns>Returns the command object.</returns>
        public static IDbCommand CreateCommand(this IDbConnection cn, FormattableString commandText)
        {
            IDbCommand rval = cn.CreateCommand();
            return rval.SetCommandText(commandText);
        }


        /// <summary>Creates a command using a command text and parameters.</summary>
        /// <param name="provider">Database provider.</param>
        /// <param name="commandText">Command text and parameters.</param>
        /// <returns>Returns the command object.</returns>
        public static IDbCommand CreateCommand(this IDbProvider provider, FormattableString commandText)
        {
            return provider.CreateCommand(commandText);
        }


        /// <summary>Creates a parameter and adds it to a command.</summary>
        /// <param name="command">Command.</param>
        /// <param name="name">Parameter name.</param>
        /// <param name="value">Parameter value.</param>
        /// <returns>Returns the created parameter.</returns>
        public static IDataParameter ApplyParameter(this IDbCommand command, string name, object? value)
        {
            IDataParameter rval = command.CreateParameter();
            rval.ParameterName = name;
            rval.Value = value;
            command.Parameters.Add(rval);

            return rval;
        }


        /// <summary>Creates a parameter and adds it to a command.</summary>
        /// <param name="command">Command.</param>
        /// <param name="name">Parameter name.</param>
        /// <param name="type">Data type.</param>
        /// <param name="value">Parameter value.</param>
        /// <returns>Returns the created parameter.</returns>
        public static IDataParameter ApplyParameter(this IDbCommand command, string name, DbType type, object? value)
        {
            IDataParameter rval = command.CreateParameter();
            rval.ParameterName = name;
            rval.DbType = type;
            rval.Value = value;
            command.Parameters.Add(rval);

            return rval;
        }


        /// <summary>Adds a parameter to a command.</summary>
        /// <param name="command">Command.</param>
        /// <param name="name">Parameter name.</param>
        /// <param name="value">Parameter value.</param>
        /// <returns>Returns the command object.</returns>
        public static IDbCommand AddParameter(this IDbCommand command, string name, object? value)
        {
            command.ApplyParameter(name, value);
            return command;
        }


        /// <summary>Adds a parameter to a command.</summary>
        /// <param name="command">Command.</param>
        /// <param name="name">Parameter name.</param>
        /// <param name="type">Data type.</param>
        /// <param name="value">Parameter value.</param>
        /// <returns>Returns the command object.</returns>
        public static IDbCommand AddParameter(this IDbCommand command, string name, DbType type, object? value)
        {
            command.ApplyParameter(name, type, value);
            return command;
        }


        /// <summary>Adds parameters to a command.</summary>
        /// <param name="command">Command.</param>
        /// <param name="parameters">Parameters as tuples of name and value.</param>
        /// <returns>Returns the command object.</returns>
        public static IDbCommand AddParameterList(this IDbCommand command, IEnumerable<(string Name, object Value)> parameters)
        {
            foreach((string Name, object Value) i in parameters) { command.ApplyParameter(i.Name, i.Value); }
            return command;
        }


        /// <summary>Adds parameters to a command.</summary>
        /// <param name="command">Command.</param>
        /// <param name="parameters">Parameters as tuples of name, type, and value.</param>
        /// <returns>Returns the command object.</returns>
        public static IDbCommand AddParameterList(this IDbCommand command, IEnumerable<(string Name, DbType Type, object Value)> parameters)
        {
            foreach((string Name, DbType Type, object Value) i in parameters) { command.ApplyParameter(i.Name, i.Type, i.Value); }
            return command;
        }


        /// <summary>Adds parameters to a command.</summary>
        /// <param name="command">Command.</param>
        /// <param name="parameters">Parameters as tuples of name and value.</param>
        /// <returns>Returns the command object.</returns>
        public static IDbCommand AddParameters(this IDbCommand command, params (string Name, object Value)[] parameters)
        {
            return command.AddParameterList(parameters);
        }


        /// <summary>Adds parameters to a command.</summary>
        /// <param name="command">Command.</param>
        /// <param name="parameters">Parameters as tuples of name, type, and value.</param>
        /// <returns>Returns the command object.</returns>
        public static IDbCommand AddParameters(this IDbCommand command, params (string Name, DbType Type, object Value)[] parameters)
        {
            return command.AddParameterList(parameters);
        }


        /// <summary>Creates a data table from a query.</summary>
        /// <param name="cn">Database connection.</param>
        /// <param name="query">Query string.</param>
        /// <returns>Data table.</returns>
        public static DataTable CreateDataTable(this IDbConnection cn, FormattableString query)
        {
            IDbCommand cmd = cn.CreateCommand(query);
            IDataReader re = cmd.ExecuteReader();

            DataTable rval = new DataTable();
            for(int i = 0; i < re.FieldCount; i++) { rval.Columns.Add(re.GetName(i), re.GetFieldType(i)); }

            while(re.Read())
            {
                DataRow r = rval.NewRow();
                for(int i = 0; i < re.FieldCount; i++)
                {
                    r[i] = re.GetValue(i);
                }
                rval.Rows.Add(r);
            }
            Disposal.Dispose(re, cmd);

            return rval;
        }


        /// <summary>Creates a data table from a query.</summary>
        /// <param name="provider">Database provider.</param>
        /// <param name="query">Query string.</param>
        /// <returns>Data table.</returns>
        public static DataTable CreateDataTable(this IDbProvider provider, FormattableString query)
        {
            return provider.Connection.CreateDataTable(query);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Replaces all occurencies of a string pattern in a string.</summary>
        /// <param name="s">String.</param>
        /// <param name="findWhat">String to be replaced.</param>
        /// <param name="replaceWith">String to replace with.</param>
        /// <param name="leftToRight">Determines if the replacement will be left to right.</param>
        /// <returns></returns>
        private static string _ReplaceDoubles(this string s, string findWhat, string replaceWith, bool leftToRight = true)
        {
            int n = 1;
            while(s.Contains(string.Concat(Enumerable.Repeat(findWhat, n)))) n++;

            for(int i = n; i > 1; i--)
            {
                Console.WriteLine("Replacing \"" + string.Concat(Enumerable.Repeat(findWhat, i)) + "\" with \"" +
                                 (leftToRight ? (replaceWith + string.Concat(Enumerable.Repeat(findWhat, i - 2))) : (string.Concat(Enumerable.Repeat(findWhat, i - 2)) + replaceWith)));
                s = s.Replace(string.Concat(Enumerable.Repeat(findWhat, i)),
                             (leftToRight ? (replaceWith + string.Concat(Enumerable.Repeat(findWhat, i - 2))) : (string.Concat(Enumerable.Repeat(findWhat, i - 2)) + replaceWith)));
            }

            return s;
        }
    }
}