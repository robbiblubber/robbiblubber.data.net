﻿using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>Classes representing DDL constraints implement this interface.</summary>
    public interface IDDLConstraint
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the constraint name.</summary>
        public string? ConstraintName { get; }


        /// <summary>Gets the table name.</summary>
        public string? TableName { get; }
    }
}
