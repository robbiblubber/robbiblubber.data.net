﻿using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>DDL statements imlement this interface.</summary>
    public interface IDDLStatement
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a statements list for this DDL.</summary>
        public string[] Text { get; }


        /// <summary>Gets a list of child DDL statements.</summary>
        public IDDLStatement[] ChildStatements { get; }
    }
}
