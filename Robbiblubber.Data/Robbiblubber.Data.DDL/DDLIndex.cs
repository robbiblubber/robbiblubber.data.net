﻿using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>This class represents a DDL index.</summary>
    public class DDLIndex: IDDLIndex, IDDLStatement
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parent DDL.</summary>
        protected DDLBuilder _Ddl;

        /// <summary>Index name.</summary>
        protected string? _IndexName;

        /// <summary>Table name.</summary>
        protected string _TableName;

        /// <summary>Column names.</summary>
        protected string[] _ColumnNames;

        /// <summary>Unique flag.</summary>
        protected bool _Unique;




        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="ddl">Parent DDL.</param>
        /// <param name="indexName">Index name.</param>
        /// <param name="tableName">Table name.</param>
        /// <param name="unique">Unique flag.</param>
        /// <param name="columnNames">Column names.</param>
        protected internal DDLIndex(DDLBuilder ddl, string? indexName, string tableName, bool unique, params string[] columnNames)
        {
            _Ddl = ddl;
            _IndexName = indexName;
            _TableName = tableName;
            _Unique = unique;
            _ColumnNames = columnNames;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="ddl">Parent DDL.</param>
        /// <param name="indexName">Index name.</param>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnNames">Column names.</param>
        protected internal DDLIndex(DDLBuilder ddl, string indexName, string tableName, params string[] columnNames): this(ddl, indexName, tableName, false, columnNames)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="ddl">Parent DDL.</param>
        /// <param name="tableName">Table name.</param>
        /// <param name="unique">Unique flag.</param>
        /// <param name="columnNames">Column names.</param>
        protected internal DDLIndex(DDLBuilder ddl, string tableName, bool unique, params string[] columnNames): this(ddl, null, tableName, unique, columnNames)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDDLStatement                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a statements list for this DDL.</summary>
        public virtual string[] Text
        {
            get { return _Ddl._Parser.ParseDDL(this); }
        }


        /// <summary>Gets a list of child DDL statements.</summary>
        IDDLStatement[] IDDLStatement.ChildStatements
        {
            get { return new IDDLStatement[0]; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDDLIndex                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the index name.</summary>
        string? IDDLIndex.IndexName 
        {
            get { return _IndexName; }
        }


        /// <summary>Gets the index table name.</summary>
        string IDDLIndex.TableName
        {
            get { return _TableName; }
        }


        /// <summary>Gets the index field names.</summary>
        string[] IDDLIndex.ColumnNames
        {
            get { return _ColumnNames; }
        }


        /// <summary>Gets if the index is unique.</summary>
        bool IDDLIndex.Unique
        {
            get { return _Unique; }
        }
    }
}
