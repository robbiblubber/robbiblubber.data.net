﻿using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>Classes representing DDL indexes implement this interface.</summary>
    public interface IDDLIndex: IDDLStatement
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the index name.</summary>
        public string? IndexName { get; }


        /// <summary>Gets the index table name.</summary>
        public string TableName { get; }


        /// <summary>Gets the index column names.</summary>
        public string[] ColumnNames { get; }


        /// <summary>Gets if the index is unique.</summary>
        public bool Unique { get; }
    }
}
