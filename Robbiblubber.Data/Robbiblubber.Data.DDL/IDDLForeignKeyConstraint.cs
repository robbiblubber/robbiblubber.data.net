﻿using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>Classes representing a DDL foreign key constraint implement this interface.</summary>
    public interface IDDLForeignKeyConstraint: IDDLConstraint
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the constraint column name.</summary>
        public string ColumnName { get; }


        /// <summary>Gets the referenced table name.</summary>
        public string ReferencedTableName { get; }


        /// <summary>Gets the referenced column name.</summary>
        public string ReferencedColumnName { get; }


        /// <summary>Gets the delete cascade option for this constraint.</summary>
        public bool CascadeDelete { get; }
    }
}
