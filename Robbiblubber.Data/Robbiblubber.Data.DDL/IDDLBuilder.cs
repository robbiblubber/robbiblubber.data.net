﻿using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>Classes that construct DDL statements implement this interface.</summary>
    public interface IDDLBuilder: IDDLStatement
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a create table statement to the DDL.</summary>
        /// <param name="tableName">Table name.</param>
        /// <returns>DDL table statement.</returns>
        public IDDLTable AddTable(string tableName);


        /// <summary>Adds a create index statement to the DDL.</summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL index statement.</returns>
        public IDDLIndex AddIndex(string tableName, params string[] columnNames);


        /// <summary>Adds a create index statement to the DDL.</summary>
        /// <param name="indexName">Index name.</param>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL index statement.</returns>
        public IDDLIndex AddNamedIndex(string indexName, string tableName, params string[] columnNames);


        /// <summary>Adds a create unique index statement to the DDL.</summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL index statement.</returns>
        public IDDLIndex AddUniqueIndex(string tableName, params string[] columnNames);


        /// <summary>Adds a create unique index statement to the DDL.</summary>
        /// <param name="indexName">Index name.</param>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnNames">Column names.</param>
        /// <returns>DDL index statement.</returns>
        public IDDLIndex AddNamedUniqueIndex(string indexName, string tableName, params string[] columnNames);
    }
}
