﻿using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>Classes representing DDL primary key constraints implement this interface.</summary>
    public interface IDDLPrimaryKeyConstraint: IDDLConstraint
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the primary key column names.</summary>
        public string[] ColumnNames { get; }
    }
}
