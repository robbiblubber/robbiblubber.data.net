﻿using System;

using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.DDL
{
    /// <summary>Classes representing DDL columns implement this interface.</summary>
    public interface IDDLColumn
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the table name.</summary>
        public string TableName { get; }


        /// <summary>Gets the column name.</summary>
        public string ColumnName { get; }


        /// <summary>Gets the column data type.</summary>
        public SQLDataType DataType { get; }


        /// <summary>Gets if the column is not null.</summary>
        public bool NotNull { get; }


        /// <summary>Gets a string representation of the default value.</summary>
        public string? DefaultValue { get; }
    }
}
