﻿using System;



namespace Robbiblubber.Data.DDL
{
    /// <summary>This class represents a foreign key constraint.</summary>
    public class DDLForeignKeyConstraint: DDLConstraint, IDDLForeignKeyConstraint, IDDLConstraint
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="table">Table.</param>
        /// <param name="constraintName">Constraint name.</param>
        /// <param name="columnName">Column name.</param>
        /// <param name="referencedTableName">Referenced table name.</param>
        /// <param name="referencedColumnName">Referenced column name.</param>
        /// <param name="cascadeDelete">Delete casecade option.</param>
        protected internal DDLForeignKeyConstraint(IDDLTable table, string constraintName, string columnName, string referencedTableName, string referencedColumnName, bool cascadeDelete = false): base(table, constraintName)
        {
            ColumnName = columnName;
            ReferencedTableName = referencedTableName;
            ReferencedColumnName = referencedColumnName;
            CascadeDelete = cascadeDelete;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="table">Table.</param>
        /// <param name="columnName">Column name.</param>
        /// <param name="referencedTableName">Referenced table name.</param>
        /// <param name="referencedColumnName">Referenced column name.</param>
        /// <param name="cascadeDelete">Delete casecade option.</param>
        protected internal DDLForeignKeyConstraint(IDDLTable table, string columnName, string referencedTableName, string referencedColumnName, bool cascadeDelete = false): base(table, null)
        {
            ColumnName = columnName;
            ReferencedTableName = referencedTableName;
            ReferencedColumnName = referencedColumnName;
            CascadeDelete = cascadeDelete;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="constraintName">Constraint name.</param>
        /// <param name="columnName">Column name.</param>
        /// <param name="referencedTableName">Referenced table name.</param>
        /// <param name="referencedColumnName">Referenced column name.</param>
        /// <param name="cascadeDelete">Delete casecade option.</param>
        protected internal DDLForeignKeyConstraint(string tableName, string? constraintName, string columnName, string referencedTableName, string referencedColumnName, bool cascadeDelete = false): base(tableName, constraintName)
        {
            ColumnName = columnName;
            ReferencedTableName = referencedTableName;
            ReferencedColumnName = referencedColumnName;
            CascadeDelete = cascadeDelete;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="columnName">Column name.</param>
        /// <param name="referencedTableName">Referenced table name.</param>
        /// <param name="referencedColumnName">Referenced column name.</param>
        /// <param name="cascadeDelete">Delete casecade option.</param>
        protected internal DDLForeignKeyConstraint(string tableName, string columnName, string referencedTableName, string referencedColumnName, bool cascadeDelete = false) : base(tableName)
        {
            ColumnName = columnName;
            ReferencedTableName = referencedTableName;
            ReferencedColumnName = referencedColumnName;
            CascadeDelete = cascadeDelete;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDDLForeignKeyConstraint                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the constraint column name.</summary>
        public virtual string ColumnName { get; protected set; }


        /// <summary>Gets the referenced table name.</summary>
        public virtual string ReferencedTableName { get; protected set; }


        /// <summary>Gets the referenced column name.</summary>
        public virtual string ReferencedColumnName { get; protected set; }


        /// <summary>Gets the delete cascade option for this constraint.</summary>
        public virtual bool CascadeDelete { get; protected set; }
    }
}
