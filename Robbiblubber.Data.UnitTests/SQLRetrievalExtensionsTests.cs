using System.Data;

using Moq;



namespace Robbiblubber.Data.UnitTests
{
    /// <summary>This class provides unit tests for SQLRetrievalExtensions.</summary>
    public class SQLRetrievalExtensionsTests
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Datetime test value.</summary>
        private DateTime _Dt = DateTime.Now;

        /// <summary>IDataReader mockup.</summary>
        private Mock<IDataReader> _Mock = new();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // test methods                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Sets up the test environment.</summary>
        [SetUp]
        public void Setup()
        {
            _Mock.Setup(m => m.IsDBNull(It.IsAny<int>())).Returns((int i) => { return (i == 0); });

            _Mock.Setup(m => m.GetValue(It.IsAny<int>())).Returns((int i) => i switch { 1 => 120,
                                                                                        2 => 18.5,
                                                                                        3 => _Dt,
                                                                                        4 => "Test",
                                                                                        5 => true,
                                                                                        6 => 4,
                                                                                        7 => "true",
                                                                                        8 => "Pernilla",
                                                                                        _ => null! });

            _Mock.Setup(m => m.GetOrdinal(It.IsAny<string>())).Returns((string c) => c.ToLower() switch { "col0" => 0,
                                                                                                          "col1" => 1,
                                                                                                          "col2" => 2,
                                                                                                          "col3" => 3,
                                                                                                          "col4" => 4,
                                                                                                          "col5" => 5,
                                                                                                          "col6" => 6,
                                                                                                          "col7" => 7,
                                                                                                          "col8" => 8,
                                                                                                          _ => -1 });
        }


        /// <summary>Implements tests for SQLRetrievalExtensions.IsNull().</summary>
        [Test][Order(0)]
        public void TestIsNull()
        {
            Assert.Multiple(() =>
            {
                Assert.That(_Mock.Object.IsNull(0), Is.True);
                Assert.That(_Mock.Object.IsNull(1), Is.False);
                Assert.That(_Mock.Object.IsNull(2), Is.False);
            });
        }


        /// <summary>Implements tests for SQLRetrievalExtensions.Get().</summary>
        [Test][Order(1)]
        public void TestGet()
        {
            Assert.Multiple(() =>
            {
                Assert.That(_Mock.Object.Get<string?>(0), Is.Null);
                Assert.That(_Mock.Object.Get<int?>(0), Is.Null);
                Assert.That(_Mock.Object.Get<double?>("col0"), Is.Null);

                Assert.That(_Mock.Object.Get<int?>(1), Is.EqualTo(120));
                Assert.That(_Mock.Object.Get<ulong>(1), Is.EqualTo(120));
                Assert.That(_Mock.Object.Get<int>("col1"), Is.EqualTo(120));

                Assert.That(_Mock.Object.Get<float?>(2), Is.EqualTo(18.5));
                Assert.That(_Mock.Object.Get<double>("col2"), Is.EqualTo(18.5));

                Assert.That(_Mock.Object.Get<DateTime?>(3), Is.EqualTo(_Dt));
                Assert.That(_Mock.Object.Get<DateTime>("col3"), Is.EqualTo(_Dt));

                Assert.That(_Mock.Object.Get<string?>(4), Is.EqualTo("Test"));
                Assert.That(_Mock.Object.Get<string>("col4"), Is.EqualTo("Test"));

                Assert.That(_Mock.Object.Get<bool?>(5), Is.True);
                Assert.That(_Mock.Object.Get<bool>("col5"), Is.True);

                Assert.That(_Mock.Object.Get<int>(6), Is.EqualTo(4));
                Assert.That(_Mock.Object.Get<bool>(6), Is.True);
                Assert.That(_Mock.Object.Get<bool>(7), Is.True);
                Assert.That(_Mock.Object.Get<bool>(4), Is.True);
                Assert.That(_Mock.Object.Get<bool>("col8"), Is.False);
            });
        }


        /// <summary>Implements tests for SQLRetrievalExtensions.Get() with defaults.</summary>
        [Test][Order(2)]
        public void TestGetWithDefault()
        {
            Assert.Multiple(() =>
            {
                Assert.That(_Mock.Object.Get<string>(0, "Marx"), Is.EqualTo("Marx"));
                Assert.That(_Mock.Object.Get<int>(0, -8), Is.EqualTo(-8));
                Assert.That(_Mock.Object.Get<string>("col8", "Marx"), Is.EqualTo("Pernilla"));
            });
        }
    }
}