﻿using System;
using System.Data;

using Moq;



namespace Robbiblubber.Data.UnitTests
{
    /// <summary>This class provides unit tests for SQLExtensions.</summary>
    public class SQLExtensionsTests
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // test methods                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Implements tests for SQLExtensions.ApplyParameter().</summary>
        [Test][Order(0)]
        public void TestApplyParameter()
        {
            var commandMock = new Mock<IDbCommand>();
            var parameterCollectionMock = new Mock<IDataParameterCollection>();
            var parameterMock = new Mock<IDbDataParameter>();

            commandMock.Setup(c => c.CreateParameter()).Returns(parameterMock.Object);
            commandMock.SetupGet(c => c.Parameters).Returns(parameterCollectionMock.Object);

            string paramName = "ParamName";
            DbType paramType = DbType.String;
            object paramValue = "ParamValue";

            commandMock.Object.ApplyParameter(paramName, paramType, paramValue);

            parameterMock.VerifySet(p => p.ParameterName = paramName, Times.Once);
            parameterMock.VerifySet(p => p.DbType = paramType, Times.Once);
            parameterMock.VerifySet(p => p.Value = paramValue, Times.Once);

            parameterCollectionMock.Verify(p => p.Add(parameterMock.Object), Times.Once);
        }


        /// <summary>Implements tests for SQLExtensions.SetCommandText().</summary>
        [Test][Order(0)]
        public void TestSetCommandText()
        {
            var commandMock = new Mock<IDbCommand>();
            var parametersMock = new Mock<IDataParameterCollection>();
            commandMock.Setup(c => c.Parameters).Returns(parametersMock.Object);
            var parameterMock = new Mock<IDataParameter>();
            var dbParameterMock = new Mock<IDbDataParameter>();
            
            commandMock.Setup(c => c.CreateParameter()).Returns(dbParameterMock.Object);
            dbParameterMock.As<IDataParameter>().SetupProperty(p => p.ParameterName);
            dbParameterMock.As<IDataParameter>().SetupProperty(p => p.Value);

            FormattableString formattableString = $"SELECT * FROM [Table] WHERE [Column] = {123} AND [OtherColumn] = {"value"}";
            commandMock.Object.SetCommandText(formattableString);

            commandMock.VerifySet(c => c.CommandText = It.IsAny<string>(), Times.Once);

            string expectedCommandText = "SELECT * FROM [Table] WHERE [Column] = :pam0 AND [OtherColumn] = :pam1";
            commandMock.VerifySet(c => c.CommandText = expectedCommandText, Times.Once);

            Assert.IsNotNull(commandMock.Object.Parameters);

            commandMock.Verify(c => c.CreateParameter(), Times.Exactly(2));
            parametersMock.Verify(p => p.Add(It.IsAny<IDataParameter>()), Times.Exactly(2));

            parametersMock.Verify(p => p.Add(It.Is<IDataParameter>(param => param.ParameterName.StartsWith(":pam") && param.Value != null)), Times.Exactly(2));
            parametersMock.Verify(p => p.Add(It.Is<IDataParameter>(param => param.ParameterName.StartsWith(":pam") && param.Value.ToString() == "value")), Times.Exactly(2));
        }
    }
}
